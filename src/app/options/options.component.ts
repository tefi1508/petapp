import { Component, OnInit } from '@angular/core';
import { NotifyServerService } from '../services/notify-server.service';

@Component({
  selector: 'app-options',
  templateUrl: './options.component.html',
  styleUrls: ['./options.component.css'],
  providers: [NotifyServerService]
})
export class OptionsComponent implements OnInit {

  buttonText: any;

  constructor(private notifySeverService: NotifyServerService) { }

  ngOnInit() {
    this.buttonText = "Alimentar mascota";
  }

  feedPet() {
    this.notifySeverService.postFeedingRequest('true').subscribe(response => {
    });
    console.log('FEED!');
    this.buttonText = "Alimentando mascota...";
  }

  refresh() {
    window.location.reload();
  }

}
