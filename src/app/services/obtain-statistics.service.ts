import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable({
  providedIn: 'root'
})
export class ObtainStatisticsService {
  url: string;

  constructor(private httpClient: HttpClient) {
    this.url = 'https://us-central1-petappserver2.cloudfunctions.net/app';
   }

  public getTimesFed() {
    return this.httpClient.get( this.url + '/getFeedTimes').map((response:any) =>{
       return response;
    });
  }
  public getPlatePercentage() {
    return this.httpClient.get( this.url + '/getPlatePercentage').map((response:any) =>{
      return response;
    });
  }
}
