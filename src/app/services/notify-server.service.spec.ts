import { TestBed } from '@angular/core/testing';

import { NotifyServerService } from './notify-server.service';

describe('NotifyServerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NotifyServerService = TestBed.get(NotifyServerService);
    expect(service).toBeTruthy();
  });
});
