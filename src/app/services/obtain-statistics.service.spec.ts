import { TestBed } from '@angular/core/testing';

import { ObtainStatisticsService } from './obtain-statistics.service';

describe('ObtainStatisticsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ObtainStatisticsService = TestBed.get(ObtainStatisticsService);
    expect(service).toBeTruthy();
  });
});
