import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';


@Injectable({
  providedIn: 'root'
})



export class NotifyServerService {

  private httpOptions: any;
  public url: string;

  constructor(private httpClient: HttpClient) {
    this.setHttpOptions();
    this.url = 'https://us-central1-petappserver2.cloudfunctions.net/app';
   }

  private setHttpOptions() {
    this.httpOptions = {
      allowedHeaders: ["Origin", "X-Requested-With", "Content-Type", "Accept", "X-Access-Token"],
      credentials: true,
      methods: "GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE",
      origin: this.url,
      preflightContinue: false
  };
  }

  public postFeedingRequest( params: any) {

   return this.httpClient.post(this.url + '/giveFood', params, this.httpOptions).map((response => {
      return response;
   }));
  }
}
