import { Component, OnInit } from '@angular/core';
import * as Chart from 'chart.js';
import { ObtainStatisticsService } from '../services/obtain-statistics.service';
import { Observable } from 'rxjs/Observable';


@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.css']
})
export class StatisticsComponent implements OnInit {

  canvas: any;
  ctx: any;
  public feedTimesValue: any;
  percentageAte: any;

  constructor(private obtainStatisticsService: ObtainStatisticsService) {
    this.getTimesFedValue();
    this.getPlatePercentage();
  }

  ngOnInit() {

  }

  getTimesFedValue() {
    this.obtainStatisticsService.getTimesFed().subscribe(response => {
      this.feedTimesValue = response;
    });
  }

  getPlatePercentage() {
    this.obtainStatisticsService.getPlatePercentage().subscribe(response => {
      this.percentageAte = response;
    })
  }

}
